from app import app
from flask import render_template
import psycopg2
import json
from psycopg2.extras import RealDictCursor
from flask import request
from multiprocessing import Process
import requests


#PUSH_SERVICE_URL = "https://bellbird.joinhandshake-internal.com/POST"


def get_rows():
    with psycopg2.connect("dbname=handshake user=postgres", cursor_factory=RealDictCursor) as conn:
        with conn.cursor() as curr:
            curr.execute("SELECT id, text, upvotes from alarms ORDER BY id DESC")
            rows = curr.fetchall()
            conn.commit()
            for r in rows:
                r["text"] = r["text"].upper()
            return rows


#def send_push_notification(alarm_id):
#    requests.post(PUSH_SERVICE_URL, data={'alarm_id': alarm_id})


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html', alarms=get_rows())


@app.route('/alarms', methods=["GET"])
def get_alarms():
    return json.dumps(get_rows())


@app.route('/alarms', methods=["POST"])
def create_new_alarm():
    with psycopg2.connect("dbname=handshake user=postgres", cursor_factory=RealDictCursor) as conn:
        with conn.cursor() as curr:
            curr.execute("INSERT INTO alarms (text) VALUES (%s) RETURNING id;", (json.loads(request.data)["text"],))
            conn.commit()

            alarm_id = curr.fetchone()['id']
            #p = Process(target=send_push_notification, args=(alarm_id,))
            #p.start()

            return json.dumps(get_rows())


@app.route('/upvotes', methods=["POST"])
def upvote():
    with psycopg2.connect("dbname=handshake user=postgres", cursor_factory=RealDictCursor) as conn:
        with conn.cursor() as curr:
            curr.execute("UPDATE alarms SET upvotes = upvotes + 1 WHERE id = %s", (json.loads(request.data)["alarm_id"],))
            conn.commit()
            return json.dumps(get_rows())
